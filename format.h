//splits the command by spaces to an array of strings


void  format(char *linein, char **argv)
{
    char* line = strrep(linein, '&', ' ');
    while (*line != '\0')         //haven't hit the end?
    {
        while (*line == ' ' || *line == '\t' || *line == '\n')
        {
            *line++ = '\0'; //replace whitespaces with null terms
        }
        *argv++ = line;          //and save the position
        while (*line != '\0' && *line != ' ' && *line != '\t' && *line != '\n')
        {
            line++;             //skip until got good text
        }
    }
    *argv = '\0';                 //and mark an end
}

