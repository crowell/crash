/**
 * 2012 - EC440 Operating System
 * Jeff Crowell
 *
 * crash.c - main entry point
 */

//includes
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <stdlib.h>
//some useful helpers here
#include "history.h" 	//struct of int, char* for history
#include "strrep.h" 	//replace instances of strings
#include "format.h"		//basically string.split(" ");
#include "checkamp.h"	//checks for & in the command 
#include "execute.h"	//execute the function
#define MAX_LINE 80 /*the maximum length command*/
#define HIST_SIZE 10 //the size of the history

//GLOBALS
char* argv[MAX_LINE];
char saved_dir[MAX_LINE];

int number = 0;
char* username;
void print_prompt()
{
	//username = getenv("USER");
	char hostname[1024];
	hostname[1023] = '\0';
	gethostname(hostname, 1023);
	printf("\033[34m%s\033[0m\033[35m@\033[0m\033[32m%s\033[0m\033[35m#\033[0m ", username, hostname); 
}

int main(int argc, char** argv)
{
	username = getenv("USER");
    int should_run = 1;
    char args[1024]; /*command line args*/
	char* ex = "exit";
	strcpy(args, ex);
    int ii;
    for(ii = 0; ii < MAX_LINE; ii++)
    {
        saved_dir[ii] = '\0';
    }
    while(should_run)
    {
	print_prompt();
	fgets(args,sizeof(args), stdin);
        if(strlen(args) == 1)
        {
            continue;
        }

        //check to see if !! syntax is correct?
        //if it is, replace args with the proper history item
        if(strncmp(args, "!!", 2) == 0)
        {
            if(number == 0)
            {
                printf("No commands in history\n");
                continue;
            }
            strcpy(args, commands[0]);//args = &commands[0];
        }
        else if(strncmp(args, "!", 1) == 0)
        {
            char num[1024] = {'\0'}; //build an array for the number char*
            int ii, jj;
            for(ii = 0; ii < strlen(args) - 1; ii++)
            {
                num[ii] = args[ii+1];	//copy over the !{num} except the !
            }
            ii = atoi(num); 			//convert back to an int
            int gotem = 0;
            for(jj = 0; jj < HIST_SIZE; jj++)
            {
                if(dex[jj] == ii)
                {
                    gotem = 1;
                    strcpy(args, commands[jj]);
                }
            }
            if(!gotem)
            {
                printf("No such command in history.\n");
                continue;
            }
        }
        //copy to a history for history
        updateHistory(args, number);
        //do the splits
        int amp = checkamp(args);
        format(args, argv);
        fflush(stdout);
        number++;
        //now fork and exec that we have everything
        if(strcmp(argv[0], "exit") == 0)
        {
            should_run = 0;
        }
        else if(strcmp(argv[0], "history") == 0)
        {
            printf("\n");
            if(number < 10)
            {
                for(ii = 0; ii < number; ii++)
                {
                    printf("%d\t%s", dex[ii], commands[ii]);
                }
            }
            else
            {
                for(ii = 0; ii < 10; ii++)
                {
                    printf("%d\t%s", dex[ii], commands[ii]);
                }
            }
            continue;
        }
		//Changing Directories
		else if(strcmp(argv[0], "cd") == 0)
		{
			if(argv[1] != '\0')
			{
				if(chdir(argv[1]) != 0)
				{
					printf("ERROR: no such directory\n");
				}
			}
			else
			{
				chdir(getenv("HOME"));
			}
		}
		else if((strcmp(argv[0], "cd ..") == 0) || strcmp(argv[0], "cd..") == 0 )
		{
			chdir("..");
		}
		//End Changing Directories
        //begin pushd/popd
        else if(strcmp(argv[0], "pushd") == 0)
        {
            char buffer[MAX_LINE];
            getcwd(buffer, MAX_LINE);
            strcpy(saved_dir, buffer); //save the dir
            if(argv[1] != '\0')
            {
                if(chdir(argv[1]) != 0)
                {
                    printf("ERROR: no such directory\n");
                }
            }
        }
        else if(strcmp(argv[0], "popd") == 0)
        {
            if(saved_dir[0] != '\0')
            {
                if(chdir(saved_dir) != 0)
                {
                    printf("ERROR: no such directory\n");
                }
            }
            saved_dir[0] = '\0';
        }
        else
        {
            execute(argv, amp);
        }
    }
    return 0;
}

