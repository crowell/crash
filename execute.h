//executes the array of arguments, but makes sure of stuff like forking

void execute(char** argv, int amp)
{
    int pid;
    int status;

    pid = fork();
    if(pid == 0) //its the child
    {
        status = execvp(*argv,argv);
        if(status < 0)
        {
            printf("ERROR!!! COULD NOT EXECUTE\n");
            exit(1);
        }
    }
    else //it is the parent
    {
        if(amp == 1) //if there is an &
        {
            while(wait(&status) != pid); //wait for the child to complete
        }
    }
}
