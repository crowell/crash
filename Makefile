all:
	gcc crash.c -o crash

clean:
	rm -f crash.o crash.exe *.stackdump crash

debug:
	gcc crash.c -g -o crash

