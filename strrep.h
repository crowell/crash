//string replace

char *trimwhitespace(char *str)
{
    char *end;

    // Trim leading space
    while(isspace(*str)) str++;

    if(*str == 0)  // All spaces?
        return str;

    // Trim trailing space
    end = str + strlen(str) - 1;
    while(end > str && isspace(*end)) end--;

    // Write new null terminator
    *(end+1) = 0;

    return str;
}

char* strrep(char *str, char old, char newc)
{
    char* ret;
    strcpy(ret, str);
    int ii;
    for(ii = 0; ii < strlen(str); ii++)
    {
        if(str[ii] == old)
        {
            ret[ii] = newc;
        }
        else
        {
            ret[ii] = str[ii];
        }
    }
    //now replace spaces at the end!
    return trimwhitespace(ret);
}

